![pandora logo](./assets/logo.png)

# Pandora

O projeto Pandora é uma caixa automatizada para recebimento e armazenamento de encomendas em residências, visando aprimorar a conveniência e a praticidade da logística em compras online. Com o avanço do comércio digital, torna-se crucial avaliar os impactos e as novas demandas dos consumidores nesse contexto. O desafio abordado é a necessidade de disponibilidade para receber as entregas ou retirá-las em agências, afetando a experiência do usuário em marketplaces.

O sistema consiste em uma caixa de entregas Pandora e um aplicativo de acompanhamento Pandora. O sistema emite um comprovante ao entregador e notifica o proprietário da caixa sobre a recepção da encomenda. O acesso e retirada das encomendas são realizados pelo usuário através de uma porta exclusiva, proporcionando uma gestão eficiente do recebimento de encomendas.

# Pandora-Firmware

Este repositório é dedicado ao desenvolvimento da parte embarcada do projeto Pandora, destinado ao funcionamento interno da própria caixa. Utilizando um conjunto composto por uma Raspberry Pi 3 Model B+ e um Arduino UNO R3, esta composição avalia o feedback de diversos sensores na caixa e atua conforme as informações obtidas. A Raspberry desempenha um papel central nesta configuração, conectando-se diretamente à API externa do nosso backend. 