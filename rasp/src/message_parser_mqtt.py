from settings import logging
import json


class MessageParserMQTT:
    def __init__(self) -> None:
        pass

    def from_mqtt(self, b_payload, topic):
        logging.debug(f"Decodifica mensagem vinda do mqtt: {b_payload}")
        try:
            data = json.loads(b_payload.decode())

            return {
                "data": data,
                "topic": topic
            }

        except Exception as error:
            logging.error(
                f"Erro ao fazer parser da mensagem vindo do mqtt: {error}")
            return None

    def to_mqtt(self, data):
        logging.debug(f"Codifica mensagem para mqtt")
        try:
            json_data = json.dumps(data)
            return f"{json_data}".encode()
        except Exception as error:
            logging.error(f"Erro ao transformar mensagem em json {error}")
