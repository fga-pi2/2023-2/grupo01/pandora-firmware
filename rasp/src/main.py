from time import sleep
from mqtt import MqttClient
from settings import init as settings_init, logging
from message_parser_mqtt import MessageParserMQTT
from package_repository import Package, PackageRepository
from serial_arduino import SerialArduino

init_loop = 0
abort_loop = 0
able_to_receive = 1
package_to_deposit = None
package_code = None
abort_thread = False

settings_init()

MAX_SEC_WAITING_DEPOSIT = 7

parser_mqtt = MessageParserMQTT()

pkRepository = PackageRepository()

logging.info(f"Conectando com arduino")
arduino = None
while not arduino:
    try:
        arduino = SerialArduino()
    except Exception:
        logging.info(f"Tentando novamente! em 1s")
        sleep(1)
        pass

logging.info(f"Conectado com arduino")


def mqtt_on_message(client, userdata, msg):
    global able_to_receive
    global init_loop
    global package_code
    global abort_loop

    logging.debug(
        f"Mensagem recebida mqtt: {msg.payload} do topico: {msg.topic}")

    data = parser_mqtt.from_mqtt(msg.payload, msg.topic)

    if data is None or not 'data' in data or not 'cmd' in data["data"]:
        logging.debug(f"Mensagem invalida!")
        return

    if data["topic"] == MqttClient.SUBSCRIBE_SERVER_TOPIC:
        logging.info(f"Recebido comando do servidor")
        # exemplo de dado recebido:
        # {
        #     cmd: save, delete, stop, return, open_back_door, open_front_door
        #     cod: 13s1,
        #     id: 123jksd
        # }

        # se recebido do topic servidor
        #   salvo na memoria codigo de encomenda
        #   recebo comando para a caixa parar funcionamento
        #   recebo comando para a caixa voltar funcionamento
        #   recebo comando para abrir a porta traseira da caixa
        #   recebo comando para abrir a porta dianteira

        if data["data"]["cmd"] == 'save':
            logging.info(f"Salvando novo codigo de encomenda na memoria")
            package = Package(
                code=data["data"]["cod"], id=data["data"]["id"])
            pkRepository.create(package)
        elif data["data"]["cmd"] == 'delete':
            logging.info(f"Deletando codigo de encomenda na memoria")
            code = data["data"]["cod"]
            pkRepository.update(code)
        elif data["data"]["cmd"] == 'stop':
            logging.info(f"Parando funcionamento da caixa")
            able_to_receive = 0
        elif data["data"]["cmd"] == 'return':
            logging.info(f"Retornando funcionamento da caixa")
            able_to_receive = 1
        elif data["data"]["cmd"] == 'open_back_door':
            logging.info(f"Abrindo porta traseira da caixa")
            arduino.request_action(arduino.OPEN_BACK_DOOR)
        elif data["data"]["cmd"] == 'open_front_door':
            logging.info(f"Abrindo porta dianteira da caixa")
            arduino.request_action(arduino.OPEN_DOOR)
        elif data["data"]["cmd"] == 'init':
            logging.info(f"Iniciando processo de deposito")
            init_loop = 1
            package_code = data["data"]["cod"]
        elif data["data"]["cmd"] == "abort":
            logging.info(f"Abortando processo de deposito")
            abort_loop = 1
        else:
            logging.warning(
                f"Recebido comando não reconhecido via mqtt {data['data']['cmd']}")

    else:
        logging.warning(
            f"Recebida mensagem de topico não esperado. topic: {data['topic']} data: {data['data']}")


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
        logging.error(f"Conexão mal sucedida com broker")
    else:
        logging.debug(f"Conexão com broker estabelecida")
        client.subscribe(MqttClient.SUBSCRIBE_SERVER_TOPIC)


client = MqttClient(on_message=mqtt_on_message, on_connect=mqtt_on_connect)
client.connect()
client.loop_start()
logging.info(f"Conectando com broker")

while not client.is_connected():
    try:
        sleep(1)
        logging.info(f"Tentando novamente! em 1s")
    except KeyboardInterrupt:
        client.loop_stop()
        client.disconnect()
        exit(1)

logging.info(f"Conectado com broker!")


def open_door_and_await_package_deposit():
    global init_loop
    global abort_loop
    successful = True

    # 4
    logging.info(f"Abrindo porta dianteira")
    arduino.request_action(arduino.OPEN_DOOR, sleep_interval=5.5)
    client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
        dict(code=client.FRONT_DOOR_OPEN)))

    # 5
    logging.info(f"Esperando entregador colocar encomenda dentro da caixa")
    count = 0
    while count < MAX_SEC_WAITING_DEPOSIT:
        sleep(2)
        # 6
        status = arduino.request_status()

        if status["is_unexpected_behavior"]:
            logging.info(
                f"Comportamento inesperado relatado pelos sensores, encerando processo de deposito! Codigo {status['num_status']}")
            client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
                dict(
                    code=client.ERROR)
            ))
            init_loop = 0
            return False
        elif status["is_package_inside"]:
            logging.info(f"Identificada encomenda dentro da caixa")
            break
        elif abort_loop == 1:
            logging.info(f"Processo abortado!")
            abort_loop = 0
            init_loop = 0
            return False
        elif not status["front_door_open"]:
            logging.info(f"Porta fechada antes de detectar encomenda!")
            # 4
            logging.info(f"Abrindo porta dianteira")
            arduino.request_action(arduino.OPEN_DOOR, sleep_interval=5.5)
            client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
                dict(code=client.FRONT_DOOR_REOPENED)))

        count += 1

    if not status["is_package_inside"]:
        # 7
        logging.info(
            f"Tempo limite atingido para deposito, encerrando processo!")
        successful = False
        client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
            dict(
                code=client.TIME_LIMIT_EXCEEDED)
        ))

    logging.info(f"Porta dianteira aberta, esperando ser fechada!")
    while status["front_door_open"]:
        if successful:
            client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
                dict(
                    code=client.CLOSE_FRONT_DOOR)
            ))

        sleep(2)
        status = arduino.request_status()
        if status["is_unexpected_behavior"]:
            logging.info(
                f"Comportamento inesperado relatado pelos sensores, encerando processo de deposito! Codigo {status['num_status']}")
            client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
                dict(
                    code=client.ERROR)
            ))
            init_loop = 0
            return False

        if abort_loop == 1:
            logging.info(f"Processo abortado!")
            abort_loop = 0
            init_loop = 0
            return False

    if not status["is_package_inside"]:
        init_loop = 0
        successful = False
        sleep(4)
        client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
            dict(
                code=client.BACK_TO_BEGIN)
        ))
        return successful

    return True


logging.info(f"Configuraçoes realizadas, esperando comandos!")
while True:
    try:
        sleep(0.3)
        if init_loop == 1 and able_to_receive == 0:
            logging.info(
                f"Caixa desabilitada, nao eh possivel inicial processo de deposito!")
            init_loop = 0
        elif init_loop == 1 and able_to_receive == 1:
            # verificar a variavel abort_loop para encerrar processo
            # 1 valida codigo recebido do tablet
            #    2 se validado ligo o sistema
            #        3 peço status dos sensores para o arduino
            #            4 se tudo certo abro a porta e aviso o tablet
            #                5 aviso o tablet que estou esperando fechar a porta
            #                6 fico pedindo status da porta para o arduino
            #                    7 se demorar mais de X segundos cancelo processo e aviso o servidor que deu ruim
            #                    8 se porta fechada começo a depositar encomenda no espaço destinado
            #                        9 aciona atuador linear
            #                           10 envio comprovante pro tablet e pro servidor
            #                           11 se deu errado o atuador aviso servidor

            # 1
            logging.info(f"Checando codigo enviado pelo tablet")
            package = pkRepository.checkCode(package_code)
            if not package:
                logging.info(f"Codigo invalido")
                client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
                    dict(code=client.INVALID_CODE)))
                init_loop = 0
                continue

            if abort_loop == 1:
                logging.info(f"Processo abortado!")
                abort_loop = 0
                init_loop = 0
                continue

            # 2
            # 3
            logging.info(f"Pedindo status dos sensores para arduino!")
            status = arduino.request_status()

            if status["is_unexpected_behavior"]:
                logging.info(
                    f"Comportamento inesperado relatado pelos sensores, encerando processo de deposito! Codigo {status['num_status']}")
                client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
                    dict(
                        code=client.ERROR)
                ))
                init_loop = 0
                continue
            elif status["is_box_full"]:
                logging.info(f"Caixa cheia!")
                client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
                    dict(code=client.BOX_FULL)
                ))
                client.publish(client.PUBLISH_SERVER_TOPIC, parser_mqtt.to_mqtt(
                    dict(msg="Caixa cheia!", error=False)
                ))
                init_loop = 0
                continue

            if abort_loop == 1:
                logging.info(f"Processo abortado!")
                abort_loop = 0
                init_loop = 0
                continue

            # 4, 5, 6, 7 e 8
            response = open_door_and_await_package_deposit()
            if not response:
                continue

            if abort_loop == 1:
                logging.info(f"Processo abortado!")
                abort_loop = 0
                init_loop = 0
                continue

            # 9
            client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
                dict(code=client.DEPOSITING_PACKAGE)
            ))
            logging.info(f"Acionando pistão para depositar encomenda!")
            response = arduino.request_action(
                arduino.TURN_ON_ENG, sleep_interval=6)
            sleep(2)

            # 10
            if response:
                client.publish(client.PUBLISH_SERVER_TOPIC, parser_mqtt.to_mqtt(
                    dict(msg="Encomenda postada!", data=dict(
                        cod=package.code, id=package.id))
                ))
            # 11
            else:
                logging.info(f"Falha ao acionar atuador linear!")
                client.publish(client.PUBLISH_SERVER_TOPIC, parser_mqtt.to_mqtt(
                    dict(msg="Falha ao acionar atuador linear!", error=True)
                ))

            logging.info(f"Processo de deposito finalizado!")
            client.publish(client.PUBLISH_TABLET_TOPIC, parser_mqtt.to_mqtt(
                dict(code=client.PACKAGE_POSTED)
            ))

            arduino.arduino.flush()

            pkRepository.update(code=package.code)
            init_loop = 0

    except KeyboardInterrupt:
        logging.info(f"Ctrl-c pressionado, encerrando programa!")
        client.loop_stop()
        client.disconnect()
        break

    except Exception as error:
        logging.error(f"Exception no loop principal: {error}")
        init_loop = 0
        logging.error(f"Processo de deposito encerrado!")
        pass
