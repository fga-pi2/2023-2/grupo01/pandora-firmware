
void setup(){
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
}
 
void loop(){
  int state = 1;
  if (Serial.available()) {
    int lido = Serial.read();

    if (state == 1) {
      state = 0;
      digitalWrite(LED_BUILTIN, HIGH);
    }
    else {
      state = 1;
      digitalWrite(LED_BUILTIN, LOW);
    }

    int toSend = 1;

    if (lido == 3) {
      toSend = 13;
    }
    else if (lido == 6) {
      toSend = 6;
    }
    else if (lido == 5) {
      toSend = 14;
    }
    else if (lido == 4) {
      toSend = 4;
    }
    else {
      toSend = lido;
    }


    Serial.println(toSend);
  }
}