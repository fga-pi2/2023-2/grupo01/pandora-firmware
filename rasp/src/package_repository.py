from io import TextIOWrapper
from settings import logging
import pickle


class Package:
    def __init__(self, code, id) -> None:
        self.code = code
        self.id = id


class PackageRepository:
    file = TextIOWrapper

    def __init__(self, file_name="data") -> None:
        self.file_name = file_name

    def init_file(self, mode):
        try:
            self.file = open(self.file_name, mode)
        except IOError as error:
            logging.warning(f"Falha em abrir arquivo: {error}")
            raise

    def close_file(self):
        try:
            if isinstance(self.file, TextIOWrapper):
                if not self.file.closed:
                    self.file.close()
        except IOError as error:
            logging.error(f"Falha em fechar arquivo: {error}")

    def create(self, package, rewrite=False):
        try:
            if not rewrite:
                packages = self.list()
                packages.append(package)
                self.init_file("wb")
                pickle.dump(packages, self.file)
                self.close_file()

            else:
                self.init_file("wb")
                pickle.dump(package, self.file)
                self.close_file()
        except Exception as error:
            logging.error(f"Falha em escrever no arquivo: {error}")

    def update(self, code):
        # a atualizacao so apaga o registro do arquivo
        try:
            self.init_file("rb")
            try:
                packages = pickle.load(self.file)
                self.close_file()
                if not packages:
                    logging.info(f"Arquivo de codigos vazio")
                    return None
                for package in packages:
                    if package.code == code:
                        self.delete(package, packages)
                        return True

            except ValueError as error:
                logging.error(
                    f"Falha em decodificar conteudo do arquivo: {error}")

        except (FileNotFoundError, EOFError):
            return False

    def delete(self, to_delete, data):
        data.remove(to_delete)
        self.create(data, rewrite=True)

    def list(self):
        try:
            self.init_file("rb")
            try:
                packages = pickle.load(self.file)
                self.close_file()
                return packages

            except ValueError as error:
                logging.error(
                    f"Falha em decodificar conteudo do arquivo: {error}")
        except (FileNotFoundError, EOFError):
            return []

    def checkCode(self, code):
        try:
            finded = False
            self.init_file("rb")
            try:
                packages = pickle.load(self.file)
                logging.debug(f"Pacotes recuperados do arquivo: {packages}")
                self.close_file()
                if not packages:
                    logging.info(f"Arquivo de codigos vazio")
                    return False
                for package in packages:
                    if package.code == code:
                        finded = package
                        break
                if finded:
                    return finded

                return False

            except ValueError as error:
                logging.error(
                    f"Falha em decodificar conteudo do arquivo: {error}")

        except (FileNotFoundError, EOFError):
            return False
