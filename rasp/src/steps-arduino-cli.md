# Commands to use arduino-cli

## create arduino sketch

arduino-cli sketch new MyFirstScketch

## open arduino file to edit

nano MyFirstScketch/MyFirstScketch.ino

## update local cache of available platforms

arduino-cli core update-index

## list connected boards

arduino-cli board list

## list installed platforms

arduino-cli core list

## install platform for arduino uno

arduino-cli core install arduino:avr

## compile sketch

arduino-cli compile --fqbn arduino:avr:uno MyFirstScketch

## upload sketch to arduino

arduino-cli upload -p /dev/ttyACM0 --fqbn arduino:avr:uno MyFirstScketch
