from package_repository import PackageRepository, Package

pkR = PackageRepository()

while True:
    try:
        char = input()
        if char == 'c':  # create
            code = input()
            package = Package(code=code, id='sfdjfnlsdnf')
            pkR.create(package=package)

        elif char == 'd':  # delete
            code = input()
            pkR.update(code=code)

        elif char == 'l':  # list
            pakages = pkR.list()
            for p in pakages:
                print(p.code, p.id)

        elif char == 'k':  # check code
            code = input()
            res = pkR.checkCode(code)
            print(res)

    except KeyboardInterrupt:
        break
