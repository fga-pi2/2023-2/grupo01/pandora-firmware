
from time import sleep
from settings import logging
import serial
import struct

MAX_ATTEMPTS = 3


class SerialArduino:
    OPEN_DOOR = 1
    OPEN_BACK_DOOR = 2
    REQUEST_SENSORS_STATUS = 8
    TURN_ON_ENG = 4

    def __init__(self, port='/dev/ttyUSB0', baud_rate=9600) -> None:
        try:
            self.arduino = serial.Serial(port,
                                         baud_rate,
                                         parity=serial.PARITY_NONE,
                                         stopbits=serial.STOPBITS_ONE,
                                         bytesize=serial.EIGHTBITS,
                                         timeout=1)
            self.arduino.flush()
        except Exception as error:
            logging.error(f"Falha ao inicializar conexão com serial {error}")
            raise

    def write(self, decimal):
        try:
            logging.debug(f"Escrevendo {decimal} na serial")
            self.arduino.write(struct.pack('>B', decimal))
        except Exception as error:
            logging.error(f"Falha ao escrever {decimal} na serial: {error}")
            raise

    def read(self):
        try:
            logging.debug(f"Lendo da serial")
            data = self.arduino.readline()
            logging.debug(data)
            data = data.decode("utf-8")
            try:
                data = int(data)
            except Exception:
                logging.warning(f"Falha em converter str para int")
            logging.debug(f"Decimal lido da serial {data}, {type(data)}")
            return data
        except IOError as error:
            logging.error(f"Falha em ler da serial: {error}")
            raise

    def parser_sensor_data(self, num):
        logging.debug(f"Parsing data da serial {num}")
        front_door_open = None
        back_door_open = None
        is_box_full = None
        is_package_inside = None
        is_unexpected_behavior = None

        if num < 9:
            front_door_open = True
        else:
            front_door_open = False

        if num in [1, 2, 3, 4, 9, 10, 11, 12]:
            back_door_open = True
        else:
            back_door_open = False

        if num in [1, 2, 5, 6, 9, 10, 13, 14]:
            is_box_full = False
        else:
            is_box_full = True

        if num % 2 == 1:
            is_package_inside = False
        else:
            is_package_inside = True

        if num in [2, 3, 4, 7, 8, 12, 16]:
            is_unexpected_behavior = True

        return {
            "front_door_open": front_door_open,
            "back_door_open": back_door_open,
            "is_box_full": is_box_full,
            "is_package_inside": is_package_inside,
            "is_unexpected_behavior": is_unexpected_behavior,
            "num_status": num,
        }

    def request_status(self):
        attempt = 0
        while attempt < MAX_ATTEMPTS:
            try:
                logging.debug(f"Pedindo status de sensores serial")
                self.write(self.REQUEST_SENSORS_STATUS)
                sleep(0.8)
                data = self.read()
                status = self.parser_sensor_data(data)
                if status["is_unexpected_behavior"]:
                    raise
                return status
            except Exception as error:
                logging.error(f"Falha ao pedir status dos sensores: {error}")
                pass
            attempt += 1
            self.arduino.flush()
            logging.warning(
                f"Tentando pedir status dos sensores via serial com arduindo, tentativa: {attempt + 1}")

        raise Exception('Falha ao comunicar com serial')

    def request_action(self, action_decimal, sleep_interval=0.3):
        attempt = 0
        while attempt < MAX_ATTEMPTS:
            try:
                logging.debug(f"Requisitando ação {action_decimal} na serial")
                self.write(action_decimal)
                sleep(sleep_interval)
                data = self.read()
                if data == action_decimal:
                    return True
            except Exception as error:
                logging.error(f"Falha ao requisitar ação ao arduino: {error}")
                pass
            attempt += 1
            self.arduino.flush()
            logging.warning(
                f"Tentando requisitar ação via serial com arduindo, tentativa: {attempt + 1}")

        raise Exception('Falha ao comunicar com serial')
