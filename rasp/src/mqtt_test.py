from time import sleep
from mqtt import MqttClient
from settings import init as settings_init, logging
from message_parser_mqtt import MessageParserMQTT

settings_init()
parser_mqtt = MessageParserMQTT()


def mqtt_on_message(client, userdata, msg):
    logging.info(f"Mensagem: {parser_mqtt.from_mqtt(msg.payload, msg.topic)}")


client = MqttClient(on_message=mqtt_on_message)
client.connect()
client.loop_start()

while not client.is_connected():
    try:
        sleep(2)
        logging.info(f"Não conectou com broker ainda")
    except KeyboardInterrupt:
        client.loop_stop()
        client.disconnect()
        exit(1)

client.subscribe(client.SUBSCRIBE_SERVER_TOPIC)
client.subscribe(client.SUBSCRIBE_TABLET_TOPIC)

counter = 1
while True:
    try:
        sleep(1)
        client.publish(client.SUBSCRIBE_TABLET_TOPIC,
                       parser_mqtt.to_mqtt(f"tablet: {counter}"))
        counter += 1
        sleep(2)
        client.publish(client.PUBLISH_SERVER_TOPIC,
                       parser_mqtt.to_mqtt(f"server: {counter}"))
    except KeyboardInterrupt:
        client.loop_stop()
        client.disconnect()
        break
