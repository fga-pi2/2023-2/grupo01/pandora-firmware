import paho.mqtt.client as mqtt_client
from settings import logging


def on_publish(client, userdata, mid):
    logging.debug(
        f"on_publish client: {client}; userdata: {userdata}; mid: {mid}")


def on_subscribe(client, userdata, mid, granted_qos):
    logging.debug(
        f"on_subscribe client: {client}; userdata: {userdata}; mid: {mid}; granted_qos: {granted_qos}")


class MqttClient:
    PUBLISH_TABLET_TOPIC = "pi2Pandora/rasp->tablet"
    PUBLISH_SERVER_TOPIC = "pi2Pandora/rasp->servidor"
    SUBSCRIBE_TABLET_TOPIC = "pi2Pandora/tablet->rasp"
    SUBSCRIBE_SERVER_TOPIC = "pi2Pandora/servidor->rasp"

    BACK_TO_BEGIN = 0
    FRONT_DOOR_OPEN = 1
    ERROR = 2
    TIME_LIMIT_EXCEEDED = 3
    CLOSE_FRONT_DOOR = 4
    INVALID_CODE = 5
    BOX_FULL = 6
    DEPOSITING_PACKAGE = 7
    PACKAGE_POSTED = 8
    FRONT_DOOR_REOPENED = 9

    def __init__(self, host="test.mosquitto.org", port=1883, keepalive=60, on_message=None, on_connect=None):
        self.client = mqtt_client.Client()
        self.host = host
        self.port = port
        self.keepalive = keepalive
        self.client.on_connect = on_connect
        self.client.on_message = on_message
        self.client.on_publish = on_publish
        self.client.on_subscribe = on_subscribe

    def connect(self):
        logging.debug(f"Conectando ao broker remoto de forma assíncrona")
        try:
            self.client.connect_async(self.host, self.port, self.keepalive)
        except (OSError, ConnectionRefusedError, ValueError) as error:
            logging.error(f"Erro ao conectar no mqtt broker: {error}")

    def disconnect(self):
        logging.debug(f"Desconecta do broker remoto")
        try:
            res = self.client.disconnect()
            if res != 0:
                logging.warning(
                    f"Desconecta retornou código diferente de 0: {res}")
        except Exception as error:
            logging.error(f"Erro ao desconectar no mqtt broker: {error}")

    def is_connected(self):
        try:
            return self.client.is_connected()
        except Exception as error:
            logging.error(f"Erro ao checar conexão com mqtt broker: {error}")
            return False

    def loop_start(self):
        logging.debug(
            f"Inicia loop de rede do broker em uma thread em segundo plano")
        try:
            res = self.client.loop_start()
            if res is not None:
                logging.warning(
                    f"Inicia loop retornou código diferente de None: {res}")
        except Exception as error:
            logging.error(f"Erro ao iniciar loop mqtt broker: {error}")

    def loop_stop(self):
        logging.debug(
            f"Para loop de rede do broker na thread em segundo plano")
        try:
            res = self.client.loop_stop()
            if res is not None:
                logging.warning(
                    f"Inicia loop retornou código diferente de None: {res}")
        except Exception as error:
            logging.error(f"Erro ao parar loop mqtt broker: {error}")

    def subscribe(self, topic):
        logging.debug(f"Inscrevendo no tópico: {topic}")
        try:
            (res, _) = self.client.subscribe(topic)
            if res is not mqtt_client.MQTT_ERR_SUCCESS:
                logging.warning(
                    f"Incrição no tópico {topic} retornou codigo diferente de 0: {res}")
        except Exception as error:
            logging.error(f"Erro ao se increver no tópico {topic}: {error}")

    def publish(self, topic, payload):
        logging.debug(f"Publicando payload {payload} para tópico {topic}")
        try:
            info = self.client.publish(topic, payload=payload)
            if info.rc != 0:
                logging.warning(
                    f"Publicação no tópico {topic} de payload {payload} retornou codigo diferente de 0: {info.rc}")
        except Exception as error:
            logging.error(f"Erro ao se increver no tópico {topic}: {error}")
