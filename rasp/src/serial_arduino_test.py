from time import sleep
from serial_arduino import SerialArduino
from settings import init as settings_init

settings_init()

arduino = SerialArduino()
i = 1
while True:
    try:
        arduino.write(i % 2)

        if i % 2 == 0:
            sleep(0.5)
        else:
            sleep(2)

        i += 1

        valor = arduino.read()
    except KeyboardInterrupt:
        exit(1)
