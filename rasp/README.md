# Pandora-Raspberry

Este repositório consiste no desenvolvimento da parte embarcada do projeto Pandora, a ser utilizado pela própria caixa. Utilizando uma Raspberry Pi 3 Model B+, um computador de placa única versátil e acessível. Graças ao seu custo acessível, conectividade Wi-Fi/Bluetooth, tamanho compacto, suporte ativo da comunidade, melhorias frequentes e seu potencial educacional para ensino de programação e eletrônica, a Raspberry Pi 3B+ destaca-se como uma escolha ideal para diversas aplicações.

## Tecnologias Utilizadas

- Raspberry Pi 3B+: Computador de placa única de baixo custo, do tamanho de um cartão de crédito, projetado para diversos usos, desde projetos educacionais até aplicações de automação residencial e IoT (Internet das Coisas).
- MQTT: (Message Queuing Telemetry Transport) é um protocolo de mensagens leve e eficiente para comunicação entre dispositivos na IoT.


## Configuração do Ambiente

### Etapas

1.  Clone o repositório para o seu ambiente local:

    `git clone https://gitlab.com/fga-pi2/2023-2/grupo01/pandora-firmware.git`

2.  Navegue até o diretório do projeto:

    `cd pandora-frontend-firmware`

3.  Instale as dependências do projeto:

    `pip install  < requirements.txt`

### Rodando o Projeto

1.  De volta ao terminal, execute o seguinte comando para iniciar o projeto:

    `python3 main.py`


## Desenvolvedores


|![Carlos Eduardo de Sousa Fiuza](https://secure.gravatar.com/avatar/15bff2b2a02bfdd3bf3422f9c8bb8bf3?s=96&d=identicon)|![Thais Rebouças de Araujo](https://gitlab.com/uploads/-/system/user/avatar/3962597/avatar.png?width=96)|
|---|---|
|Carlos Fiuza|Thais Araujo|
