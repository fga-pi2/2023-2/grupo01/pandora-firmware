# Pandora-Arduino

Este repositório consiste no desenvolvimento da parte embarcada do projeto Pandora, a ser utilizado pela própria caixa. Utilizando um Arduino UNO R3, uma placa de desenvolvimento incrível para estudantes devido à sua facilidade de aprendizado e versatilidade. Ele oferece uma introdução prática à eletrônica e à programação, permitindo que os alunos construam projetos interativos de forma rápida e simples. A interface amigável do Arduino e sua compatibilidade com uma variedade de sensores e componentes tornam-no uma escolha popular para projetos educacionais como este.

## Tecnologias Utilizadas

- Arduino UNO R3: Uma placa de desenvolvimento utilizada bastante por estudantes pela sua facilidade de aprendizado em eletrônica e programação, além de sua versatilidade para criar projetos interativos e práticos no campo de ciência e tecnologia.

## Configuração do Ambiente

### Pré-Requisitos

Tenha instalado a IDE do Arduino no sistema operacional que será utilizado para executar o código. Você pode encontrar a IDE no site: https://www.arduino.cc/en/software/

### Etapas

1.  Clone o repositório para o seu ambiente local:

    `git clone https://gitlab.com/fga-pi2/2023-2/grupo01/pandora-firmware.git`

2.  Navegue até o diretório do projeto:

    `cd pandora-frontend-firmware`

3. Abra a IDE e selecione a placa:

    `Arduino UNO R3`

4. Selecione a porta serial a qual o Arduino será conectado e o conecte.

5. Compile o código [Pandora_simulado.ino](https://gitlab.com/fga-pi2/2023-2/grupo01/pandora-firmware/-/blob/main/arduino/Pandora_simulado.ino?ref_type=heads) na IDE e faça o upload para o Arduino

Garanta que o Arduino esteja conectado corretamente na porta serial escolhida.

### Rodando o Projeto

Uma vez que o código [Pandora_simulado.ino](https://gitlab.com/fga-pi2/2023-2/grupo01/pandora-firmware/-/blob/main/arduino/Pandora_simulado.ino?ref_type=heads) esteja no Arduino, toda vez que ele estiver conectado a uma fonte de energia 12v, ele funcionará sempre que estiver ligado.


## Desenvolvedores

|![Henrique Moura](https://secure.gravatar.com/avatar/15bff2b2a02bfdd3bf3422f9c8bb8bf3?s=96&d=identicon)|![Yuri César](https://secure.gravatar.com/avatar/15bff2b2a02bfdd3bf3422f9c8bb8bf3?s=96&d=identicon)|
|---|---|
|Henrique Moura|Yuri César|
