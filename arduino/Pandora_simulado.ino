// Código Caixa de Pandora
// Simulação do circuito completo
// Falta a integração com a RASP, apenas
// A comunicação da RASP com o ARDO será
// realizada através de um nipple


const int IN2 = 2;
const int IN3 = 3;
bool flag;
// Nipple Arduino <=> Raspberry
const int bit1 = 16;
const int bit2 = 17;
const int bit3 = 18;
const int bit4 = 19;
const int bit5 = 11;
// Travas magnéticas - diant. e tras.
const int LED1 = 14;
const int LED2 = 15;
// Magneticos
const int MG4 = 4;
const int MG5 = 5;
const int MG12 = 12;
const int MG13 = 13;
// Sensor Ultrasônico
const byte trigger_pin1 = 6;
const byte echo_pin1 = 7;
const byte trigger_pin2 = 10;
const byte echo_pin2 = 9;

unsigned long duration1;
unsigned long distance1;
unsigned long duration2;
unsigned long distance2;

// Funções

void stop_motor() {
  digitalWrite(IN2,LOW);
  digitalWrite(IN3,LOW);
}

void reset_motor() {
  
 digitalWrite(IN2, HIGH);
 digitalWrite(IN3, LOW);
 do { // nothing ...
 } while (digitalRead(MG4) == HIGH);
 stop_motor();
}

void end_motor() {
  
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, HIGH);
    do { // nothing ...
    } while (digitalRead(MG5) == HIGH);
    stop_motor();
}

bool motor_run() {
  
  if (flag) {
    reset_motor(); // voltar o pistao
    end_motor(); // avançar o pistao
    reset_motor(); // voltar o pistao
  	stop_motor(); // parar o pistao
  }
  flag = false;
}

void restart() {
  
  digitalWrite(LED1,LOW);
  digitalWrite(LED2,LOW);
  stop_motor();
  flag = true;
}  


void abrir_porta_diant() {
  
  digitalWrite(LED1,HIGH);
}

void fechar_porta_diant() {
  
  digitalWrite(LED1,LOW);
}

void fechar_porta_tras() {
  
  digitalWrite(LED2,LOW);
}

void abrir_porta_tras() {
  digitalWrite(LED2,HIGH);
}

int raspberry_pi() {
  
  int b1 = !digitalRead(bit1);
  int b2 = !digitalRead(bit2);
  int b3 = !digitalRead(bit3);
  int b4 = !digitalRead(bit4);
  int b5 = !digitalRead(bit5);
  
  int base10 = b1*4 + b2*2 + b3*1 + b5*8;
  
  return base10;
}

void setup() {
  
  TCCR1B = TCCR1B & 0b11111000 | 0x01; // Clock de 1 KHz
 
  // Sensores de fim de curso
  pinMode(MG4,INPUT_PULLUP);
  pinMode(MG5,INPUT_PULLUP);
  pinMode(MG12,INPUT_PULLUP);
  pinMode(MG13,INPUT_PULLUP);
  
  // RASP
  pinMode(bit1,INPUT_PULLUP);
  pinMode(bit2,INPUT_PULLUP);
  pinMode(bit3,INPUT_PULLUP);
  pinMode(bit4,INPUT_PULLUP);
  pinMode(bit5,INPUT_PULLUP);
 
  // Travas magnéticas
  pinMode(LED1,OUTPUT);
  pinMode(LED2,OUTPUT);  
  digitalWrite(LED1,OUTPUT);
  digitalWrite(LED2,OUTPUT);
  
  // Controle do motor
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  flag = true;
  
  // Sensor HC-SR04
  pinMode(echo_pin1,INPUT);
  pinMode(trigger_pin1,OUTPUT);
  pinMode(echo_pin2,INPUT);
  pinMode(trigger_pin2,OUTPUT);
 
  Serial.begin(9600);
}

void loop() {
  
  int input= 0;
  if(Serial.available())
      input = Serial.read();
  else  
      input = raspberry_pi();  
    
    // States
    if (input==1) {
      Serial.println(input);
      abrir_porta_diant();
      delay(5000);
      fechar_porta_diant();
    }
    else if(input==2) {
      Serial.println(input);
      abrir_porta_tras();
      delay(5000);
      fechar_porta_tras();
    }
    else if(input==4) {
      motor_run();
      Serial.println(input);
      delay(200);
    }    
    else if(input==8) {
      
      int output= 1;

      // Sensor entrega de encomenda
      digitalWrite(trigger_pin1, LOW);
      delayMicroseconds(2);
      digitalWrite(trigger_pin1, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigger_pin1, LOW);

      duration1 = pulseIn(echo_pin1, HIGH);
      distance1 = (duration1*0.0343)/2;    

      // Sensor caixa cheia
      digitalWrite(trigger_pin2, LOW);
      delayMicroseconds(2);
      digitalWrite(trigger_pin2, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigger_pin2, LOW);

      duration2 = pulseIn(echo_pin2, HIGH);
      distance2= (duration2*0.0343)/2;    
      
      if (distance1 < 34)
        output += 1;
          
      if (distance2 < 20)
          output += 2;
      
      int s1= digitalRead(MG12);
      int s2= digitalRead(MG13);

      if (s2 == LOW)
        output += 4;

      if (s1 == LOW)
        output += 8;  

      Serial.println(output);
      delay(2000);
    }
    else
    {
      restart();
      delay(200);
    } 
}